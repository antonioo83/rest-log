<?php
return array(
    'restLog' => array(
        'isActive' => true,
        'maxCodeCount' => 1,
        'databaseConfig' => include (__DIR__ . '/database-config.php')
    )
);