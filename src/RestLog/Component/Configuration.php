<?php
namespace RestLog\Component;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('restLog');

        $rootNode
            ->children()
                ->arrayNode('databaseConfig')
                    ->children()
                        ->scalarNode('host')
                            ->defaultValue('localhost')
                        ->end()
                        ->scalarNode('driver')
                            ->defaultValue('pdo_mysql')
                        ->end()
                        ->scalarNode('user')
                            ->isRequired()
                        ->end()
                        ->scalarNode('password')
                            ->isRequired()
                        ->end()
                        ->scalarNode('dbname')
                            ->isRequired()
                        ->end()
                        ->scalarNode('charset')
                            ->defaultValue('UTF8')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('defaultCommandSettings')
                    ->prototype('array')
                        ->children()
                            ->booleanNode('isActive')
                                ->defaultValue(true)
                            ->end()
                            ->enumNode('transport')
                                ->values(\RestLog\Service\Type\TransportType::getAllConstants())
                                ->defaultValue(\RestLog\Service\Type\TransportType::DATABASE)
                            ->end()
                            ->scalarNode('maxPackageSize')
                                ->defaultValue(10*1024*1024)
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('commandSettings')
                    ->prototype('array')
                        ->children()
                            ->integerNode('companyId')
                                ->defaultValue(null)
                            ->end()
                            ->integerNode('sourceId')
                                ->defaultValue(null)
                            ->end()
                            ->booleanNode('isActive')
                                ->defaultValue(true)
                            ->end()
                            ->enumNode('transport')
                                ->values(\RestLog\Service\Type\TransportType::getAllConstants())
                                ->defaultValue(\RestLog\Service\Type\TransportType::DATABASE)
                            ->end()
                            ->integerNode('maxPackageSize')
                                ->min(1024)
                                ->max(5*1024*1024*1024)
                                ->defaultValue(10*1024*1024)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

}