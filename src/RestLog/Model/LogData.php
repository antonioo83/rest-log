<?php
namespace RestLog\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogData
 *
 * @ORM\Table(name="lg_log_data", indexes={@ORM\Index(name="logId", columns={"logId"})})
 * @ORM\Entity
 */
class LogData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="blob", length=16777215, nullable=false)
     */
    public $data;

    /**
     * @var Log
     *
     * @ORM\ManyToOne(targetEntity="Log")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="logId", referencedColumnName="id")
     * })
     */
    public $log = null;

    /**
     * @return Log
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param Log $log
     */
    public function setLog(Log $log)
    {
        $this->log = $log;
    }

}

