<?php
namespace RestLog\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 *
 * @ORM\Table(name="lg_log", indexes={@ORM\Index(name="companyId", columns={"companyId", "sourceId", "userId", "eventCreatedAt"}), @ORM\Index(name="companyId_2", columns={"companyId", "createdAt"}), @ORM\Index(name="createdAt", columns={"createdAt"})})
 * @ORM\Entity
 */
class Log
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="companyId", type="bigint", nullable=true)
     */
    public $companyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sourceId", type="integer", nullable=true)
     */
    public $sourceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    public $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    public $level = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="commandType", type="string", length=10, nullable=false)
     */
    public $commandType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", length=20, nullable=false)
     */
    public $command = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="count", type="integer", nullable=false)
     */
    public $count = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    public $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=100, nullable=false)
     */
    public $entity = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=false)
     */
    public $title = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="responseCode", type="smallint", nullable=true)
     */
    public $responseCode = null;

    /**
     * @var string
     *
     * @ORM\Column(name="application", type="string", length=10, nullable=false)
     */
    public $application = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sessionId", type="string", length=32, nullable=false)
     */
    public $sessionId = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="executedAt", type="datetime", nullable=false)
     */
    public $executedAt = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="microsecExecutedAt", type="decimal", precision=15, scale=4, nullable=false)
     */
    public $microsecExecutedAt = 0.0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    public $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="microsecCreatedAt", type="decimal", precision=15, scale=4, nullable=false)
     */
    public $microsecCreatedAt = 0.0;    

    /**
     * @var boolean
     *
     * @ORM\Column(name="iteration", type="float", nullable=false)
     */
    public $iteration = 0;

    /**
     * Log constructor.
     */
    public function __construct()
    {
        $this->executedAt = new \DateTime();
        $this->createdAt = new \DateTime();
    }

}

