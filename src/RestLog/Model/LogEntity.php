<?php
namespace RestLog\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogEntity
 *
 * @ORM\Table(name="lg_log_entity", indexes={@ORM\Index(name="companyId", columns={"companyId", "sourceId", "createdAt", "entity", "code"}), @ORM\Index(name="companyId_2", columns={"companyId", "createdAt", "sourceId", "entity", "appCode"}), @ORM\Index(name="logId", columns={"logId"})})
 * @ORM\Entity
 */
class LogEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="companyId", type="bigint", nullable=true)
     */
    public $companyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sourceId", type="integer", nullable=true)
     */
    public $sourceId;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=100, nullable=false)
     */
    public $entity = '';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32, nullable=true)
     */
    public $code;

    /**
     * @var string
     *
     * @ORM\Column(name="appCode", type="string", length=32, nullable=true)
     */
    public $appCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    public $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var Log
     *
     * @ORM\ManyToOne(targetEntity="Log")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="logId", referencedColumnName="id")
     * })
     */
    public $log = null;

    /**
     * @return Log
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param Log $log
     */
    public function setLog(Log $log)
    {
        $this->log = $log;
    }

}

