<?php
namespace RestLog\Service;

use RestLog\Component\Configuration;
use Symfony\Component\Config\Definition\Processor;

class ConfigService extends BaseService
{
    /**
     * @var array
     */
    private $config = array();

    /**
     * @var \RestLog\Component\Configuration|null
     */
    private $configuration = null;

    /**
     * @var array
     */
    private $defaultCompanySettings = array();

    /**
     * @var array
     */
    private $companySettings = array();

    /**
     * ConfigService constructor.
     *
     * @param \RestLog\Component\Configuration|null $configuration
     */
    public function __construct(\RestLog\Component\Configuration $configuration, $config)
    {
        $this->configuration = $configuration;
        $this->config = $config;
    }

    /**
     * @return $this
     */
    public function loadConfig()
    {
        $this->defaultCompanySettings = $this->getConfig()['defaultCompanySettings'];
        $this->companySettings = $this->getConfig()['defaultCompanySettings'];
        
        return $this;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        $processor = new Processor();
        try {
            $this->setConfig(
                $processor->processConfiguration(new Configuration(), $this->config)
            );
        } catch (\Exception $ex) {
            $this->setErrorMessage($ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

}