<?php
namespace RestLog\Service\Transport\Interfaces;

use RestLog\Service\BaseLogService;

interface BaseInterface
{
    /**
     * @param $companyId
     * @param null $userId
     * @param null $sourceId
     * @return $this
     */
    public function init($companyId, $userId = null, $sourceId = null);

    /**
     * @return int
     */
    public function getIteration();

    /**
     * @param $iteration
     *
     * @return BaseLogService
     */
    public function setIteration($iteration);

}