<?php
namespace RestLog\Service\Transport\Interfaces;

interface MessageLogInterface extends BaseInterface
{
    /**
     * @param $message
     * @return $this
     * @throws \Exception
     */
    public function error($message);

    /**
     * @param $message
     * @return mixed
     */
    public function info($message);

    /**
     * @param $message
     * @return mixed
     */
    public function debug($message);

    /**
     * @param $message
     * @return mixed
     */
    public function critical($message);

}