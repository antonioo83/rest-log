<?php
namespace RestLog\Service\Transport\Interfaces;

use RestLog\Service\Transport\AppLogService;

interface RestLogStorageInterface extends BaseInterface
{
    /**
     * @param $data
     * @param array $codes
     * @param string $sessionId
     * @return $this|AppLogService
     */
    public function setData($data, array $codes = array(), $sessionId = '');

    /**
     * @return string
     */
    public function getCodeProperty();

    /**
     * @return array
     */
    public function getCodes();

    /**
     * @param $count
     * @return mixed
     */
    public function setCount($count);

}