<?php
namespace RestLog\Service\Transport;

use RestLog\Service\BaseRestLogService;

class RestLogService extends BaseRestLogService
{
    private $codes = array();

    /**
     * @param $data
     * @param array $codes
     * @param string $sessionId
     * @return $this|RestLogService
     */
    public function setData($data, array $codes = array(), $sessionId = '')
    {
        $this->setJsonData($data);
        $this->codes = $codes;
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeProperty()
    {
        return 'code';
    }    

    /**
     * @return array
     */
    public function getCodes()
    {
        return $this->codes;
    }

}