<?php
namespace RestLog\Service\Transport;

use RestLog\Service\BaseRestLogService;

class AppLogService extends BaseRestLogService
{
    private $appCodes = array();

    /**
     * @param $data
     * @param array $codes
     * @param string $sessionId
     * @return $this|AppLogService
     */
    public function setData($data, array $codes = array(), $sessionId = '')
    {
        $this->jsonData = ($this->isJson($data)) ? $data : json_encode($data);
        $this->appCodes = $codes;
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeProperty()
    {
        return 'appCode';
    }

    /**
     * @return array
     */
    public function getCodes()
    {
        return $this->appCodes;
    }

}