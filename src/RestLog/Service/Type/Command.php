<?php
namespace RestLog\Service\Type;


class Command extends BaseType
{
    const CREATE = 'CREATE';

    const UPDATE = 'UPDATE';

    const DELETE = 'DELETE';

    const REPLACE = 'REPLACE';

    const GET = 'GET';

    const GET_ITEMS = 'GET_ITEMS';

    const UPDATE_ITEMS = 'UPDATE_ITEMS';

    const REPLACE_CALLBACK = 'REPLACE_CALLBACK';

    const CALLBACK = 'CALLBACK';

}