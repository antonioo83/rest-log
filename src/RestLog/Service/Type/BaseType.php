<?php
namespace RestLog\Service\Type;

abstract class BaseType
{
    /**
     * @return array
     */
    public static function getAllConstants()
    {
        $class = new \ReflectionClass(static::class);

        return $class->getConstants();
    }

}