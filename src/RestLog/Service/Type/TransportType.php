<?php
namespace RestLog\Service\Type;


class TransportType extends BaseType
{
    const DATABASE = 'db';

    const FILE = 'file';
}