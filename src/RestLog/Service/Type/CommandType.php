<?php
namespace RestLog\Service\Type;


class CommandType extends BaseType
{
    const REQUEST = 'REQUEST';

    const RESPONSE = 'RESPONSE';

    const CALLBACK_REQUEST = 'CALLBACK_REQUEST';

    const CALLBACK_RESPONSE = 'CALLBACK_RESPONSE';

    const MESSAGE = 'MESSAGE';

    const EXCEPTION = 'EXCEPTION';
}