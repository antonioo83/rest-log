<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210812123900 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE `lg_log` MODIFY `level` TINYINT(3) NOT NULL DEFAULT 1;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
