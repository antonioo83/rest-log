<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210205155628 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('lg_log');
        $table->addColumn(
            'microsecCreatedAt',
            \Doctrine\DBAL\Types\Type::DECIMAL,
            array('notnull' => true, 'precision' => 15, 'scale' => 4,'default' => '0.0000')
        );
        $table->addColumn('executedAt', \Doctrine\DBAL\Types\Type::DATETIME, array('notnull' => true, 'default' => '0000-00-00 00:00:00'));
        $table->addColumn(
            'microsecExecutedAt',
            \Doctrine\DBAL\Types\Type::DECIMAL,
            array('notnull' => true, 'precision' => 15, 'scale' => 4,'default' => '0.0000')
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
